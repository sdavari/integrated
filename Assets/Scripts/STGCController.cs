﻿using UnityEngine;
using GoogleVR.Beta;
using System;

public enum OverlapMode { ManualMoveUp = 1, AutoTransparentize = 2, Notify = 3,
	ManualMoveDown = 4, ManualTransparentizeTransparent = 5, Auto_Move = 6,
	ManualTransparentizeOpaque = 7, NotifyMove = 8
};

public class STGCController : MonoBehaviour
{
	public GameObject kid;
	public OverlapMode mode;
	public bool change_mode;

	void Start()
	{
		//Main
		bool supported = GvrBetaSettings.IsFeatureSupported(GvrBetaFeature.SeeThrough);
		bool enabled = GvrBetaSettings.IsFeatureEnabled(GvrBetaFeature.SeeThrough);
		if (supported && !enabled)
		{
			GvrBetaFeature[] features = new GvrBetaFeature[] { GvrBetaFeature.SeeThrough };
			GvrBetaSettings.RequestFeatures(features, null);
		}

		GvrCardboardHelpers.Recenter();
		change_mode = false;
		mode = (OverlapMode)GameObject.Find("ChosenMethodMenue").GetComponent<ChangeScene>().getChosenMethod();

		//Destroy
		Destroy(GameObject.Find("ChosenMethodMenue"));
		Destroy(GameObject.Find("MenueCanvas"));
	}

	public OverlapMode getMode()
	{
		return mode;
	}

	private void Update()
	{
		// to Test change mode manually
		if (change_mode)
		{
			CycleModes();
			change_mode = false;
		}
		foreach (var hand in Gvr.Internal.ControllerUtils.AllHands)
		{
			GvrControllerInputDevice device = GvrControllerInput.GetDevice(hand);
			// if press App Button
			if (device.GetButtonUp(GvrControllerButton.App))
			{
				GameObject tv = GameObject.Find("TV");
				if (tv.GetComponent<resetTV>().isMethodChanger())
				{
					GameObject.Find("TrialManager").GetComponent<TrialManager>().trialSetNum++;
					tv.GetComponent<resetTV>().methodChanger_set(false);
					GameObject.Find("STGCController").GetComponent<STGCController>().CycleModes();
				}
				
			}
		}
	}

	public void CycleModes()
	{
		switch (mode)
		{

			case OverlapMode.ManualMoveUp:
				mode = OverlapMode.AutoTransparentize;
				break;

			case OverlapMode.AutoTransparentize:
				mode = OverlapMode.Notify;
				break;

			case OverlapMode.Notify:
				mode = OverlapMode.ManualMoveDown;
				break;

			case OverlapMode.ManualMoveDown:
				mode = OverlapMode.ManualTransparentizeTransparent;
				break;

			case OverlapMode.ManualTransparentizeTransparent:
				mode = OverlapMode.Auto_Move;
				break;

			case OverlapMode.Auto_Move:
				mode = OverlapMode.ManualTransparentizeOpaque;
				break;

			case OverlapMode.ManualTransparentizeOpaque:
				mode = OverlapMode.NotifyMove;
				break;

			case OverlapMode.NotifyMove:
				mode = OverlapMode.ManualMoveUp;
				break;
		}
		GameObject.Find("TrialManager").GetComponent<TrialManager>().ChangeMethod();
	}

	private void OnDestroy()
	{
		// Disable see-through when this scene ends.
		GvrBetaHeadset.SetSeeThroughConfig(GvrBetaSeeThroughCameraMode.Disabled,
			GvrBetaSeeThroughSceneType.Virtual);
	}

	public static implicit operator GameObject(STGCController v)
	{
		throw new NotImplementedException();
	}
}

/*
 * 
 * // IN TRAINING IF CLICKED APP TWICE SKIPS IT:
				if (tv.GetComponent<resetTV>().is_Training() && tv.GetComponent<resetTV>().isMethodChanger())
				{
					
					tv.GetComponent<resetTV>().methodChanger_set(false);
					tv.GetComponent<resetTV>().isTraining_set(false);
					GameObject.Find("TrialManager").GetComponent<TrialManager>().trialSetNum++;
					GameObject.Find("TrialManager").GetComponent<TrialManager>().ChangeMethod();

				}
				else
				if(tv.GetComponent<resetTV>().is_Training())
				{
					tv.GetComponent<resetTV>().methodChanger_set(true);
					GameObject.Find("TrialManager").GetComponent<TrialManager>().ChangeMethod();
					
				}
 * */
