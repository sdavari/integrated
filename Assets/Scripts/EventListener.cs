﻿using UnityEngine;
using UnityEngine.EventSystems;


public class EventListener : MonoBehaviour
{
	public GameObject gc;
	private Renderer _renderer;
	private Renderer _renderer_BlurNoise;
	private OverlapMode mode;
	private float initialX, initialZ;
	private float upY = .60f;
	private float slideY = .03f;
	private float transparentColor = 0.08f;

	void Start()
	{
		if (gc)
		{
			_renderer = gc.GetComponent<Renderer>();
			initialX = gc.transform.localPosition.x;
			initialZ = gc.transform.localPosition.z;
		}
	}

	public void reverse_transparency()
	{
		Color color = _renderer.material.color;

		GCNBCommonVariables commonVariables = gc.GetComponent<GCNBCommonVariables>();
		UserIntendedGCState state = commonVariables.getUser_intended();
		
		if (state == UserIntendedGCState.Opaque)
		{
			color.a = transparentColor;
		}
		else if (state == UserIntendedGCState.Transparrent)
		{
			color.a = 1.0f;
		}
		_renderer.material.color = color;
		commonVariables.reverse_user_intended();
	}

	public void slide_manually()
	{
		GCNBCommonVariables commonVariables = gc.GetComponent<GCNBCommonVariables>();
		UserIntendedGCState state = commonVariables.getUser_intended();
		if (state == UserIntendedGCState.Unmoved)
		{
			commonVariables.slideUp();

		}
		else
		{
			commonVariables.slideToCenter();
		}
	}

	public void OnClick(BaseEventData eventData)
	{
		if (gameObject.tag == "Exit")
		{
			if (Application.platform == RuntimePlatform.Android)
			{
				AndroidJavaObject activity = new AndroidJavaClass("com.unity3d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentActivity");
				activity.Call<bool>("moveTaskToBack", true);
			}
			else
			{
				Application.Quit();
			}
			GameObject.Find("TrialManager").GetComponent<TrialManager>().writer.WriteLine("E");
			GameObject.Find("TrialManager").GetComponent<TrialManager>().writer.Close();
		}
		else if (gameObject.tag == "Ceiling")
		{
			GameObject kid = GameObject.Find("Kid");
			KidActivityStatus kidActivityStatus = kid.GetComponent<KidActivityStatus>();
			if (kidActivityStatus.getKidStatus() == KidStatus.Wave)
			{
				kidActivityStatus.setWaveback(true);
				kidActivityStatus.setKidStatus(KidStatus.Idle);
			}
			else
			{
				GameObject.Find("TrialManager").GetComponent<TrialManager>().writer.WriteLine("U " + Time.time);
			}
		}
		else if (gameObject.tag == "TV")
		{
			if (GetComponent<resetTV>().isTrialChanger())
			{
				GameObject.Find("TrialManager").GetComponent<TrialManager>().nxt_trial();
				GetComponent<resetTV>().trialChanger_set(false);
			}
		}
		else 
		{
			GameObject tm = GameObject.Find("TrialManager");
			tm.GetComponent<TrialManager>().writer.WriteLine("C " + get_appNum(transform.parent.name[0]));
			mode = tm.GetComponent<TrialManager>().getMode();

			if (mode != OverlapMode.ManualMoveDown && mode != OverlapMode.ManualMoveUp && mode != OverlapMode.Auto_Move && mode != OverlapMode.NotifyMove)
			{
				reverse_transparency();
				if (mode == OverlapMode.AutoTransparentize)
					GetComponent<GCNBCommonVariables>().wasClickedOn();
			}
			if (mode == OverlapMode.ManualMoveDown || mode == OverlapMode.ManualMoveUp || mode == OverlapMode.Auto_Move || mode == OverlapMode.NotifyMove)
			{
				slide_manually();
				if (mode == OverlapMode.Auto_Move)
					GetComponent<GCNBCommonVariables>().wasClickedOn();
			}
		}
	}

	private int get_appNum (char firstletter)
	{
		switch (firstletter)
		{
			case 'I':
				return 1;
			case 'F':
				return 2;
			case 'W':
				return 3;
			case 'G':
				return 4;
			case 'S':
				return 5;
			case 'N':
				return 6;
			default:
				return 0;
		}
	}

	void Update()
	{
		if (mode == OverlapMode.ManualMoveDown || mode == OverlapMode.ManualMoveUp || mode == OverlapMode.Auto_Move || mode == OverlapMode.NotifyMove)
		{
			GCNBCommonVariables commonVariables = gc.GetComponent<GCNBCommonVariables>();
			if (commonVariables.is_sliding_up())
			{
				if (gc.transform.localPosition.y < upY)
				{
					gc.transform.localPosition = new Vector3(initialX, gc.transform.localPosition.y + slideY, initialZ);
					commonVariables.answerObj.transform.localPosition = new Vector3(commonVariables.getAnswerInitialPos().x, commonVariables.answerObj.transform.localPosition.y + slideY, commonVariables.getAnswerInitialPos().z);
				}
				else
				{
					gc.transform.localPosition = new Vector3(initialX, upY, initialZ);
					commonVariables.answerObj.transform.localPosition = new Vector3(commonVariables.getAnswerInitialPos().x, commonVariables.upYAns, commonVariables.getAnswerInitialPos().z);
					commonVariables.stopSlide();
				}
			}
			if (commonVariables.is_sliding_2_center())
			{
				if (gc.transform.localPosition.y > 0)
				{
					gc.transform.localPosition = new Vector3(initialX, gc.transform.localPosition.y - slideY, initialZ);
					commonVariables.answerObj.transform.localPosition = new Vector3(commonVariables.getAnswerInitialPos().x, 
						commonVariables.answerObj.transform.localPosition.y - slideY, 
						commonVariables.getAnswerInitialPos().z);
				}
				else
				{
					gc.transform.localPosition = new Vector3(initialX, 0, initialZ);
					commonVariables.answerObj.transform.localPosition = commonVariables.getAnswerInitialPos();
					commonVariables.stopSlide();
				}
			}
		}
	}
}

