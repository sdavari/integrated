﻿using System.IO;
using UnityEngine;

public enum KidStatus { Idle = 1,
						Walk = 2,
						Wave = 3 };

public class KidActivityStatus : MonoBehaviour
{
	private KidStatus kidStatus;
	private bool waveBack;
	private bool answeredQuestion;

	public void Reset()
	{
		kidStatus = KidStatus.Idle;
		waveBack = true;
		answeredQuestion = true;
	}

	public KidStatus getKidStatus()
	{
		return kidStatus;
	}

	public bool is_waved_back()
	{
		return waveBack;
	}

	public bool is_question_answered()
	{
		return answeredQuestion;
	}

	public void setKidStatus(KidStatus v)
	{
			
		kidStatus = v;

		if (kidStatus == KidStatus.Wave)
		{
			setWaveback(false);
			GameObject.Find("TrialManager").GetComponent<TrialManager>().writer.WriteLine("W " + Time.time);
			//GetComponent<AudioSource>().enabled = true;
		}
	}

	public void setWaveback(bool v)
	{
		if(kidStatus == KidStatus.Wave && v)
			GameObject.Find("TrialManager").GetComponent<TrialManager>().writer.WriteLine("B " + Time.time);
		waveBack = v;
	}

	public void setansweredQuestion(bool v)
	{
		answeredQuestion = v;
	}

}
