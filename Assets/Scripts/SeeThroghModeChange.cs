﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GoogleVR.Beta;
using UnityEngine.EventSystems;

public class SeeThroghModeChange : MonoBehaviour
{
	
	public GameObject[] hideDuringSeeThrough;
		
	//Cycle See Thgrough Mode
	public void OnClick(BaseEventData eventData)
	{
		if (!GvrBetaSettings.IsFeatureEnabled(GvrBetaFeature.SeeThrough))
		{
			return;
		}
		GvrBetaSeeThroughCameraMode camMode = GvrBetaHeadset.CameraMode;
		GvrBetaSeeThroughSceneType sceneType = GvrBetaSeeThroughSceneType.Augmented;

		switch (camMode)
		{
		case GvrBetaSeeThroughCameraMode.Disabled:
			camMode = GvrBetaSeeThroughCameraMode.RawImage;
			break;
		case GvrBetaSeeThroughCameraMode.RawImage:
			sceneType = GvrBetaSeeThroughSceneType.Virtual;
			camMode = GvrBetaSeeThroughCameraMode.Disabled;
			break;
		}

		GvrBetaHeadset.SetSeeThroughConfig(camMode, sceneType);

		bool seethruEnabled = camMode != GvrBetaSeeThroughCameraMode.Disabled;
		//replace with tag
		foreach (var go in hideDuringSeeThrough)
		{
			go.SetActive(!seethruEnabled);
		}
	}
}
