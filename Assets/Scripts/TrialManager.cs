﻿using UnityEngine;
using UnityEngine.Video;
using TMPro;
using System;
using System.IO;
using UnityEngine.UI;

public enum QuestionType
{
	Insta = 1,
	FitBit = 2,
	Weather = 3,
	Gmail = 4,
	Snapchat = 5,
	News = 6,
	None = 7,
};

public class TrialManager : MonoBehaviour
{
	public bool tvstandclicked = false;
	public TextAsset[] trialsets;
	public GameObject[] gCNBs;
	public GameObject kid;
	public TextMeshProUGUI question;
	public GameObject answerToggles;
	
	// Constants
	private const int totalNumberofTrials = 10;
	private const int totalNumberofTrainingTrials = 14;
	private const float upY = .6f;
	private const float transparentColor = 0.08f;
	private const int variablesNum = 5; 
	private const int EndTime = 3; 
	// General 
	private OverlapMode mode;
	private KidActivityStatus kidActivityStatus;
	// Scene setup
	private GameObject tv;
	// Trials Sets and Trial Num managemnet
	public int trialSetNum = 0;
	private int trialNum = 1;
	
	//File IO
	private string iOPath;
	public StreamWriter writer;
	private string[] lines;

	// Trial Variables 
	private Vector3 trialKidStartPosition;
	private Quaternion trialKidStartRotation;
	private float TrialTimeForEnd;
	// Read from Files
	private QuestionType trialsQuestionType;
	private int trialsAnswer;
	private bool trialHasWave;
	private float trialTimeForStartActivity;
	private int trialKidwin;

	void Start()
	{
		tv = GameObject.Find("TV");

		kidActivityStatus = kid.GetComponent<KidActivityStatus>();
		trialSetNum = 0;
		startNextTrialSet();
		kid.transform.position = new Vector3(-4.45f, 0f, -3.4f);
		kid.transform.rotation = Quaternion.Euler(0, 150, 0);
	}

	private void Update()
	{
		// For Debuging
		if (tvstandclicked)
		{
			tvstandclicked = false;
			nxt_trial();
		}
		// if after running the Experiment both were answered:
		if (kidActivityStatus.is_question_answered() && kidActivityStatus.is_waved_back() && trialTimeForStartActivity == float.PositiveInfinity)
		{
			if (TrialTimeForEnd == float.PositiveInfinity)
			{
				TrialTimeForEnd = Time.time + EndTime;
			}
			if (Time.time >= TrialTimeForEnd)
			{
				if (trialSetNum != 0)
				{
					if (0 < trialNum && trialNum <= totalNumberofTrials)
						AskQuestion("nxt");
					else if (trialNum == totalNumberofTrials)
					{
						AskQuestion("nxtSet");
					}
				}
				else
				{
					if (0 < trialNum && trialNum < totalNumberofTrainingTrials)
						AskQuestion("train");
					else if (trialNum == totalNumberofTrainingTrials)
					{
						AskQuestion("train2");
					}
				}
			}
		}
		if ( Time.time >= trialTimeForStartActivity)
		{
			if (trialHasWave)
				kidActivityStatus.setKidStatus(KidStatus.Wave);
			changeContentAndAsk();
			trialTimeForStartActivity = float.PositiveInfinity;
			TrialTimeForEnd = float.PositiveInfinity;
		}
	}

	public void ChangeMethod()
	{
		mode = GameObject.Find("STGCController").GetComponent<STGCController>().getMode();
		startNextTrialSet();
	}

	public void startNextTrialSet()
	{
		if (trialSetNum > 8)
			Application.Quit();
		ResetContent();
		get_IOfile();
		// For Logs
		writer.WriteLine("M " + (int)mode);
		writer.WriteLine("S " + trialSetNum.ToString());
		trialTimeForStartActivity = float.PositiveInfinity;
		TrialTimeForEnd = float.PositiveInfinity;
		trialNum = 0;
		if (trialSetNum != 0)
		{

			AskQuestion("first");
		}
		else
		{
			AskQuestion("trainS");	
		}
	}

	public void nxt_trial()
	{
		trialNum++;
		writer.WriteLine("N " + Time.time);
		ResetContent();
		if ( ( trialSetNum != 0 && trialNum <= totalNumberofTrials) || (trialSetNum == 0 && trialNum <= totalNumberofTrainingTrials ))
		{
			for (int varInd = 1; varInd < variablesNum + 1; varInd++)
			{
				int startindx = ((trialNum - 1) * variablesNum) + varInd - 1;
				string line = lines[startindx];

				switch (varInd)
				{
					case 1:
						trialsQuestionType = (QuestionType)int.Parse(line);
						break;
					case 2:
						trialsAnswer = int.Parse(line) % 4;
						break;
					case 3:
						trialHasWave = ('y' ==  line[0]);
						break;
					case 4:
						trialTimeForStartActivity = int.Parse(line) + Time.time;
						break;
					case 5:
						trialKidwin = int.Parse(line);
						set_trialKidStartPosAndRot();
						break;
					default:
						break;
				}
			}
			kidActivityStatus.setKidStatus(KidStatus.Idle);
		}
		else
		{
			if(trialSetNum != 0 )
				AskQuestion("nxtSet");
			else
				AskQuestion("train2");
		}
	}

	private void set_trialKidStartPosAndRot()
	{
		switch (trialKidwin)
		{
			case 1:
				trialKidStartPosition = new Vector3(-6.56f, 0.0f, -5.4f);
				trialKidStartRotation = Quaternion.Euler(0, 90, 0);
				break;
			case 2:
				trialKidStartPosition = new Vector3(-6.42f, 0.0f, -3.9f);
				trialKidStartRotation = Quaternion.Euler(0, 120, 0);
				break;
			case 3:
				trialKidStartPosition = new Vector3(-5.5f, 0.0f, -2.95f);
				trialKidStartRotation = Quaternion.Euler(0, 140, 0);
				break;
			case 4:
				trialKidStartPosition = new Vector3(-0.6f, 0.0f, -3.0f);
				trialKidStartRotation = Quaternion.Euler(0, 200, 0);
				break;
			case 5:
				trialKidStartPosition = new Vector3(0.0f, 0.0f, -3.755f);
				trialKidStartRotation = Quaternion.Euler(0, 210, 0);
				break;
			case 6:
				trialKidStartPosition = new Vector3(0.08f, 0.0f, -5.36f);
				trialKidStartRotation = Quaternion.Euler(0, 230, 0);
				break;
			default:
				break;

		}
		kid.transform.position = trialKidStartPosition;
		kid.transform.rotation = trialKidStartRotation;
	}

	public void ResetContent()
	{
		if (trialSetNum != 0)
			mode = GameObject.Find("STGCController").GetComponent<STGCController>().getMode();
		else // If it's the start of experience's training
			mode = OverlapMode.ManualMoveDown;
		kidActivityStatus.Reset();

		//Reset TV
		tv.GetComponent<resetTV>().clicked_on_TV(); 
		answerToggles.SetActive(false);

		// Reset GC
		for (int index = 0; index < gCNBs.Length; index++)
		{
			GameObject gc = gCNBs[index].transform.Find("GCScreen").gameObject;
			//Reset Answers:
			resetAnswersToInitial(index);
			GCNBCommonVariables commonVariables = gc.GetComponent<GCNBCommonVariables>();
			commonVariables.reset(mode);
			if (mode == OverlapMode.ManualTransparentizeTransparent)
			{
				//Opeque
				make_opaque(gc, false);
			}
			else
			{
				make_opaque(gc, true);
			}

			//in initial location in terms of y
			if (mode != OverlapMode.ManualMoveUp)
			{
				gc.transform.localPosition = new Vector3(gc.transform.localPosition.x, 0.0f , gc.transform.localPosition.z);
				commonVariables.answerObj.transform.localPosition = commonVariables.getAnswerInitialPos();
			}
			else
			{
				gc.transform.localPosition = new Vector3(gc.transform.localPosition.x, upY, gc.transform.localPosition.z);
				commonVariables.answerObj.transform.localPosition = new Vector3(commonVariables.getAnswerInitialPos().x, commonVariables.upYAns, commonVariables.getAnswerInitialPos().z);

			}
			//Notif off
			turn_off_notification(gc);
		}
	}

	private void changeToggleAnswersOnTV()
	{
		turnOffToggles();
		switch (trialsQuestionType)
		{
			case QuestionType.Insta:
				answerToggles.transform.GetChild(0).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "24";
				answerToggles.transform.GetChild(1).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "245";
				answerToggles.transform.GetChild(2).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "453";
				answerToggles.transform.GetChild(3).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "45";
				break;
			case QuestionType.FitBit:	
				answerToggles.transform.GetChild(0).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "50";
				answerToggles.transform.GetChild(1).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "150";
				answerToggles.transform.GetChild(2).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "500";
				answerToggles.transform.GetChild(3).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "510";
				break;
			case QuestionType.Weather:
				answerToggles.transform.GetChild(0).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "73";
				answerToggles.transform.GetChild(1).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "37";
				answerToggles.transform.GetChild(2).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "38";
				answerToggles.transform.GetChild(3).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "83";
				break;
			case QuestionType.Gmail:
				answerToggles.transform.GetChild(0).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "15";
				answerToggles.transform.GetChild(1).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "73";
				answerToggles.transform.GetChild(2).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "37";
				answerToggles.transform.GetChild(3).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "51";
				break;
			case QuestionType.Snapchat:
				answerToggles.transform.GetChild(0).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "Elen";
				answerToggles.transform.GetChild(1).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "Jon";
				answerToggles.transform.GetChild(2).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "Tom";
				answerToggles.transform.GetChild(3).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "Emma";
				break;
			case QuestionType.News:
				answerToggles.transform.GetChild(0).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "32";
				answerToggles.transform.GetChild(1).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "2";
				answerToggles.transform.GetChild(2).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "23";
				answerToggles.transform.GetChild(3).transform.Find("Label").GetComponent<TextMeshProUGUI>().text = "3";
				break;
			case QuestionType.None:
				break;
		}

	}

	private void changeContentAndAsk()
	{
		if (trialsQuestionType != QuestionType.None)
		{
				changeContent();
			writer.WriteLine("Q " + (int)trialsQuestionType + " " + trialsAnswer + " " + Time.time);
		}
		switch (trialsQuestionType)
		{
			case QuestionType.Insta:	
				AskQuestion("Instagram: How many likes did the last post on your Instagram feed receive?");
				break;
			case QuestionType.FitBit:
				AskQuestion("FitBit: How many calories are left to consume today?");
				break;
			case QuestionType.Weather:
				AskQuestion("Weather: what will the temperature be today at 3 pm?");
				break;
			case QuestionType.Gmail:
				AskQuestion("Email: How many unread emails are there in All Inboxes?");
				break;
			case QuestionType.Snapchat:
				AskQuestion("Snapchat: Who is the last person who uploaded a story?");
				break;
			case QuestionType.News:
				AskQuestion("News: How many hours ago was the last trending story from Huffpost posted?");
				break;
			case QuestionType.None:
				break;
		}
	}

	private void changeContent()
	{
		// Change Answer on GC
		string address = "GC/" + trialsQuestionType.ToString() + "/" + trialsAnswer;
		gCNBs[(int)trialsQuestionType - 1].transform.Find("GCScreen").gameObject.GetComponent<GCNBCommonVariables>().resetAnswerSprites(address.Trim());
		changeToggleAnswersOnTV();
	}

	private void resetAnswersToInitial(int gcIndex)
	{
		QuestionType QType = (QuestionType)gcIndex + 1;
		string address = "GC/" + QType.ToString() + "/0";
		gCNBs[gcIndex].transform.Find("GCScreen").gameObject.GetComponent<GCNBCommonVariables>().resetAnswerSprites(address.Trim());
		answerToggles.SetActive(false);
	}

	public void AskQuestion(string trialQ)
	{
		kidActivityStatus.setansweredQuestion(false);
		tv.GetComponent<VideoPlayer>().Pause();
		question.enabled = true;
		if (trialQ == "first") //First Trial
		{
			question.text = "Training: Method " + (int) mode  + ": \n Please familiarize yourself with this method! \n Click on the TV whenever you are ready to start the first trial!";
			tv.GetComponent<resetTV>().trialChanger_set(true);
		}
		else if (trialQ == "nxt") //Nxt Trial
		{
			question.text = "Please click on the TV whenever you are ready to start the next trial!";
			tv.GetComponent<resetTV>().trialChanger_set(true);
		}
		else if (trialQ == "nxtSet")
		{
			question.text = "You have reached the end of this set of trials! Please take off the head set and answer to the Questionnaire.";
			tv.GetComponent<resetTV>().methodChanger_set(true);
		}
		else if (trialQ == "trainS")
		{
			question.text = "Training: Please familiarize yourself with the digital content and environment! \n Click on the Tv to start the first training trial.";
			tv.GetComponent<resetTV>().trialChanger_set(true);
		}
		else if (trialQ == "train")
		{
			question.text = "Training: Please familiarize yourself with the digital content and environment! \n Click on the Tv to start the next training trial.";
			tv.GetComponent<resetTV>().trialChanger_set(true);
		}
		else if (trialQ == "train2")
		{
			question.text = "Training: Please familiarize yourself with the digital content and environment! \n Let the researcher know whenever you are ready to start the experiment!";
			//tv.GetComponent<resetTV>().trialChanger_set(true);
			tv.GetComponent<resetTV>().methodChanger_set(true);
		}
		else
		{
			answerToggles.SetActive(true);
			question.text = trialQ;
		}
	}
	   
	private void make_opaque(GameObject gc, bool opaque)
	{
		GCNBCommonVariables gCNBCommon = gc.GetComponent<GCNBCommonVariables>();
		Renderer _renderer = gc.GetComponent<Renderer>();
		Color color = _renderer.material.color;
		if (opaque)
			color.a = 1.0f;
		else
			color.a = transparentColor;
		gCNBCommon.setAnswerTransparency();
		_renderer.material.color = color;
	}

	private void turn_off_notification(GameObject gc)
	{
		SpriteRenderer _sr = gc.GetComponent<SpriteRenderer>();
		string spriteName = _sr.sprite.name;
		string endstr = spriteName.Substring(Math.Max(0, spriteName.Length - 8));
		Sprite nonOutlinedSprite;
		if (endstr.Equals("Outlined"))
			nonOutlinedSprite = Resources.Load<Sprite>("GC/" + spriteName.Substring(0, spriteName.Length - 8));
		else
			nonOutlinedSprite = Resources.Load<Sprite>("GC/" + spriteName);
		_sr.sprite = nonOutlinedSprite;
	}

	private void get_IOfile()
	{
		if (trialSetNum == 0)
		{
			string n = string.Format("training-{0:yyyy-MM-dd_hh-mm-ss-tt}_TrialSet_{1:d}.txt", DateTime.Now , trialSetNum);
			if (Application.platform == RuntimePlatform.Android)
			{
				writer = new StreamWriter(Application.persistentDataPath + "/" + n);
			}
			else
			{
				writer = new StreamWriter("Assets/OutputFiles/" + n);
			}

		}
		else
		{
			writer.Close();
			string n = string.Format("text-{0:yyyy-MM-dd_hh-mm-ss-tt}_TrialSet_{1:d}.txt", DateTime.Now, trialSetNum);

			if (Application.platform == RuntimePlatform.Android)
			{
				writer = new StreamWriter(Application.persistentDataPath + "/"+ n);
			}
			else
			{
				writer = new StreamWriter("Assets/OutputFiles/" + n);
			}
		}
		StreamReader reader = new StreamReader(new MemoryStream(trialsets[trialSetNum].bytes));
		string content = reader.ReadToEnd();
		reader.Close();
		lines = content.Split("\n"[0]);
	}

	private void turnOffToggles()
	{
		for(int i = 0; i < 4; i++)
			answerToggles.transform.GetChild(i).GetComponent<Toggle>().isOn = false;
	}

	public OverlapMode getMode()
	{
		return mode;
	}

	private void OnDestroy()
	{
		writer.Close();
	}
}

