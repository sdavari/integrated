﻿using UnityEngine;
using TMPro;
using UnityEngine.Video;
using UnityEngine.UI;

public class resetTV : MonoBehaviour
{
	public TextMeshProUGUI question;
	public GameObject answerToggles;
	private bool methodChanger = false;
	private bool trialChanger = false;

	public void trialChanger_set(bool v)
	{
		trialChanger = v;
	}

	public bool isTrialChanger()
	{
		return trialChanger;
	}

	public void methodChanger_set(bool v)
	{
		methodChanger = v;
	}

	public bool isMethodChanger()
	{
		return methodChanger;
	}

	public void choseAnswer(int ans)
	{
		// If not question but prompts: add the answer clicked on to writer
		ans--;
		if (GameObject.Find("Canvas").transform.Find("AnswerToggles").GetChild(ans).GetComponent<Toggle>().isOn)
			GameObject.Find("TrialManager").GetComponent<TrialManager>().writer.WriteLine("A " + (ans) + " " + Time.time);
		answerToggles.SetActive(false);
		trialChanger = false;
		GameObject.Find("Kid").GetComponent<KidActivityStatus>().setansweredQuestion(true);
		question.enabled = false;
		GameObject.Find("TV").GetComponent<VideoPlayer>().Play();

	}

	public void clicked_on_TV()
	{
		trialChanger = false;
		GameObject.Find("Kid").GetComponent<KidActivityStatus>().setansweredQuestion(true);
		question.enabled = false;
		gameObject.GetComponent<VideoPlayer>().Play();
	}
}
