﻿using UnityEngine;
using UnityEngine.XR;

public class MeshBuilder : MonoBehaviour {
	private Mesh mesh;
	private Vector3[] vertices;
	private int[] triangles;
	private Vector2[] uvs;

	public GameObject gc;
	public GameObject gcNb;


	//private Vector3 eyeCenter;
	//private Matrix4x4 m;
	private Vector3 vertLeftTopFront;
	private Vector3 vertLeftBottomFront;
	private Vector3 vertRightTopFront;
	private Vector3 vertRightBottomFront;

	private Vector3 vertLeftTopBack;
	private Vector3 vertRightTopBack;
	private Vector3 vertLeftBottomBack;
	private Vector3 vertRightBottomBack;

	private Vector3 leftBottomFront;
	private Vector3 rightTopFront;
	private Vector3 rightBottomFront;
	private Vector3 leftTopFront;

	void Start () {
		mesh = GetComponent<MeshFilter> ().mesh;
		gameObject.AddComponent<MeshCollider>();
		gameObject.GetComponent<MeshCollider>().convex = true;
		gameObject.GetComponent<MeshCollider>().isTrigger = true;

		transform.localScale = Vector3.one;
		transform.localPosition = Vector3.zero;
		transform.localRotation = Quaternion.identity;

		//Triangles (clockwise)
		triangles = new int[]{
			//front face
			//triangle 1
			0, 2, 3,
			//triangle 2
			3, 1, 0,

			//back face
			//triangle 1
			4, 6, 7,
			//triangle 2
			7, 5, 4,

			//left face
			//triangle 1
			8, 10, 11,
			//triangle 2
			11, 9, 8,

			//right face
			//triangle 1
			12, 14, 15,
			//triangle 2
			15, 13, 12,

			//top face
			//triangle 1
			16, 18, 19,
			//triangle 2
			19, 17, 16,

			//bottom face
			//triangle 1
			20, 22, 23,
			//triangle 2
			23, 21, 20
		};

		//UVs
		uvs = new Vector2[] {
			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0), 

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0),

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0),

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0),

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0),

			//front face
			// bottom left 0, 0
			// top right 1, 1
			new Vector2(0 ,1),
			new Vector2(0 ,0),
			new Vector2(1 ,1),
			new Vector2(1 ,0)
		};

		// gc corners
		SpriteRenderer sr = gc.GetComponent<SpriteRenderer>();
		if (gcNb.transform.rotation.y > 0.2f)
		{
			leftTopFront = new Vector3(sr.bounds.min.x, sr.bounds.max.y, sr.bounds.max.z);
			rightBottomFront = new Vector3(sr.bounds.max.x, sr.bounds.min.y, sr.bounds.min.z);
			rightTopFront = new Vector3(sr.bounds.max.x, sr.bounds.max.y, sr.bounds.min.z);
			leftBottomFront = new Vector3(sr.bounds.min.x, sr.bounds.min.y, sr.bounds.max.z);
		}
		else
		{
			leftTopFront = new Vector3(sr.bounds.min.x, sr.bounds.max.y, sr.bounds.min.z);
			rightBottomFront = new Vector3(sr.bounds.max.x, sr.bounds.min.y, sr.bounds.max.z);
			rightTopFront = new Vector3(sr.bounds.max.x, sr.bounds.max.y, sr.bounds.max.z);
			leftBottomFront = new Vector3(sr.bounds.min.x, sr.bounds.min.y, sr.bounds.min.z);

		}

		vertLeftBottomFront = transform.InverseTransformPoint(leftBottomFront);
		vertLeftTopFront = transform.InverseTransformPoint(leftTopFront);
		vertRightBottomFront = transform.InverseTransformPoint(rightBottomFront);
		vertRightTopFront = transform.InverseTransformPoint(rightTopFront);

	}
	
	void Update () {
		GetComponent<MeshCollider>().enabled = false;
		Matrix4x4 m = Camera.main.cameraToWorldMatrix;
		Vector3 eyeCenter = m.MultiplyPoint(Quaternion.Inverse(InputTracking.GetLocalRotation(XRNode.CenterEye)) * InputTracking.GetLocalPosition(XRNode.CenterEye)); // Eye center in world coordinates
		vertLeftBottomBack = eyeCenter + 15 * (leftBottomFront - eyeCenter);
		vertRightTopBack = eyeCenter + 15 * (rightTopFront - eyeCenter);
		vertRightBottomBack = eyeCenter + 15 * (rightBottomFront - eyeCenter);
		vertLeftTopBack = eyeCenter + 15 * (leftTopFront - eyeCenter);

		vertLeftBottomBack = transform.InverseTransformPoint(vertLeftBottomBack);
		vertRightTopBack = transform.InverseTransformPoint(vertRightTopBack);
		vertRightBottomBack = transform.InverseTransformPoint(vertRightBottomBack);
		vertLeftTopBack = transform.InverseTransformPoint(vertLeftTopBack);


		vertices = new Vector3[]{
			//front face
			vertLeftTopFront, //0 [left x=-1, top y=1, front z=1]
			vertRightTopFront, //1 [right x=1, top y=1, front z=1]
			vertLeftBottomFront, //2 [left x=-1, bottom y=-1, front z=1]
			vertRightBottomFront, //3 [right x=1, bottom y=-1, front z=1]

			//back face
			vertRightTopBack, //4 [right x=1, top y=1, back z=-1]
			vertLeftTopBack, //5 [left x=-1, top y=1, back z=-1]
			vertRightBottomBack, //6 [right x=1, bottom y=-1, back z=-1]
			vertLeftBottomBack, //7 [left x=-1, bottom y=-1, back z=-1]

			//left face
			vertLeftTopBack, //8 [left x=-1, top y=1, back z=-1]
			vertLeftTopFront, //9 [left x=-1, top y=1, frontz=1]
			vertLeftBottomBack, //10 [left x=-1, bottom y=-1, back z=-1]
			vertLeftBottomFront, //11 [left x=-1, bottom y=-1, front z=1]

			//right face
			vertRightTopFront, //12 [right x=1, top y=1, front z=1]
			vertRightTopBack, //13 [right x=1, top y=1, back z=-1]
			vertRightBottomFront, //14 [right x=1, bottom y=-1, front z=1]
			vertRightBottomBack, //15 [right x=1, bottom y=-1, back z=-1]

			//top face
			vertLeftTopBack, //16 [left x=-1, top y=1, back z=-1]
			vertRightTopBack, //17 [right x=1, top y=1, back z=-1]
			vertLeftTopFront, //18 [left x=-1, top y=1, front z=1]
			vertRightTopFront, //19 [right x=1, top y=1, front z=1]

			//bottom face
			vertLeftBottomFront, //20 [left x=-1, bottom y=-1, front z=1]
			vertRightBottomFront, //21 [right x=1, bottom y=-1, front z=1]
			vertLeftBottomBack, //22 [left x=-1, bottom y=-1, back z=-1]
			vertRightBottomBack //23 [right x=1, bottom y=-1, back z=-1]
			};

		mesh.Clear();
		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.uv = uvs;
		mesh.RecalculateNormals();
		mesh.RecalculateBounds();
		mesh.Optimize();
		GetComponent<MeshCollider>().enabled = true;
	}

	/*
	private void OnDrawGizmos ()
	{
		if(true)//(gcNb.gameObject.name == "FitBitGCNB" || gcNb.gameObject.name == "GmailGCNB" || gcNb.gameObject.name == "WeatherGCNB")
		{
			m = Camera.main.cameraToWorldMatrix;
			eyeCenter = m.MultiplyPoint(Quaternion.Inverse(InputTracking.GetLocalRotation(XRNode.CenterEye)) * InputTracking.GetLocalPosition(XRNode.CenterEye)); // Eye center in world coordinates

			// gc corners
			SpriteRenderer sr = gc.GetComponent<SpriteRenderer>();

			//Debug.Log(gcNb.gameObject.name + " : " + gcNb.transform.rotation.y);
			if (gcNb.transform.rotation.y > 0.0f)
			{
				leftTopFront = new Vector3(sr.bounds.min.x, sr.bounds.max.y, sr.bounds.max.z);
				rightBottomFront = new Vector3(sr.bounds.max.x, sr.bounds.min.y, sr.bounds.min.z);
				rightTopFront = new Vector3(sr.bounds.max.x, sr.bounds.max.y, sr.bounds.min.z);
				leftBottomFront = new Vector3(sr.bounds.min.x, sr.bounds.min.y, sr.bounds.max.z);
			}
			else
			{
				leftTopFront = new Vector3(sr.bounds.min.x, sr.bounds.max.y, sr.bounds.min.z);
				rightBottomFront = new Vector3(sr.bounds.max.x, sr.bounds.min.y, sr.bounds.max.z);
				rightTopFront = new Vector3(sr.bounds.max.x, sr.bounds.max.y, sr.bounds.max.z);
				leftBottomFront = new Vector3(sr.bounds.min.x, sr.bounds.min.y, sr.bounds.min.z);

			}
			vertLeftBottomBack = eyeCenter + 15 * (leftBottomFront - eyeCenter);
			vertRightTopBack = eyeCenter + 15 * (rightTopFront - eyeCenter);
			vertRightBottomBack = eyeCenter + 15 * (rightBottomFront - eyeCenter);
			vertLeftTopBack = eyeCenter + 15 * (leftTopFront - eyeCenter);

			//Debug.Log(gcNb.gameObject.name + " : " +  Mathf.Sign( transform.rotation.w));

			Gizmos.color = Color.red;
			Gizmos.DrawCube(leftBottomFront, .05f * Vector3.one);
			Gizmos.DrawLine(eyeCenter, eyeCenter + 15 * (leftBottomFront - eyeCenter));

			Gizmos.color = Color.yellow;
			Gizmos.DrawCube(leftTopFront, .05f * Vector3.one);
			Gizmos.DrawLine(eyeCenter, eyeCenter + 15 * (leftTopFront - eyeCenter));

			Gizmos.color = Color.blue;
			Gizmos.DrawCube(rightBottomFront, .05f * Vector3.one);
			Gizmos.DrawLine(eyeCenter, eyeCenter + 15 * (rightBottomFront - eyeCenter));

			Gizmos.color = Color.green;
			Gizmos.DrawLine(eyeCenter, eyeCenter + 15 * (rightTopFront - eyeCenter));
			Gizmos.DrawCube(rightTopFront, .05f * Vector3.one);

			Gizmos.DrawCube(eyeCenter, .05f * Vector3.one);		
		}
	}
	
	*/

}
