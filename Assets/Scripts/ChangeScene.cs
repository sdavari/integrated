﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
	public int modeNum;
	public static int chosenMethod;
	public bool clicked = false;

	private void Awake()
	{
		DontDestroyOnLoad(this);
	}

	private void Start()
	{
		clicked = false;

	}
	
	private void Update()
	{
		if (clicked)
			changeToScene(modeNum);
		
	}

	public void changeToScene(int mode)
	{
		modeNum = mode;
		chosenMethod = mode;
		SceneManager.LoadScene(1);
		GameObject.Find("MenueCanvas").SetActive(false);
	}

	public int getChosenMethod()
	{
		return modeNum;
	}

}
