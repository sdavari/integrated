﻿using UnityEditor;
using UnityEngine;

public enum UserIntendedGCState { Transparrent, Opaque, Moved, Unmoved};
public enum SlideDirection { None, Up, ToCenter };
public class GCNBCommonVariables : MonoBehaviour
{
	public GameObject answerObj;
	public float upYAns;
	private Vector3 answerInitialPos;
	private SlideDirection slideDirection;
	private UserIntendedGCState user_intended;
	private bool clickedOn = false;

	private void Awake()
	{
		answerInitialPos = answerObj.transform.localPosition;
	}

	public Vector3 getAnswerInitialPos()
	{
		return answerInitialPos;
	}

	public void transparentAns()
	{
		Color color = answerObj.GetComponent<Renderer>().material.color;
		color.a = 0.0f;
		answerObj.GetComponent<Renderer>().material.color = color;
	}

	public void opaqueAns()
	{
		Color color = answerObj.GetComponent<Renderer>().material.color;
		color.a = 1.0f;
		answerObj.GetComponent<Renderer>().material.color = color;
	}


	public void setAnswerTransparency()	
	{
		if (user_intended == UserIntendedGCState.Transparrent || user_intended == UserIntendedGCState.Moved)
		{
			transparentAns();
		}
		else
		{
			opaqueAns();
		}
	}

	public void setAnswerBlurer()
	{
		setAnswerTransparency();
	}

	public void resetAnswerSprites(string address)
	{
		answerObj.GetComponent<SpriteRenderer>().sprite = Resources.Load<Sprite>(address.Trim());
		setAnswerBlurer();
	}

	public void reset(OverlapMode mode)
	{
		clickedOn = false;
		slideDirection = SlideDirection.None;
		switch (mode)
		{

			case OverlapMode.ManualMoveUp:
				setUser_intended(UserIntendedGCState.Moved);
				break;

			case OverlapMode.ManualTransparentizeOpaque:
			case OverlapMode.AutoTransparentize:
			case OverlapMode.Notify:
				setUser_intended(UserIntendedGCState.Opaque);
				break;

			case OverlapMode.ManualTransparentizeTransparent:
				setUser_intended(UserIntendedGCState.Transparrent);
				break;

			case OverlapMode.Auto_Move:
			case OverlapMode.NotifyMove:	
			case OverlapMode.ManualMoveDown:
				setUser_intended(UserIntendedGCState.Unmoved);
				break;
			default:
				setAnswerTransparency();
				break;
		}
	}

	public void wasClickedOn()
	{
		clickedOn = true;
	}

	public bool getClickedOn()
	{
		return clickedOn;
	}

	public void slideUp()
	{
		setUser_intended(UserIntendedGCState.Moved);
		slideDirection = SlideDirection.Up;
	}

	public void stopSlide()
	{
		slideDirection = SlideDirection.None;
	}

	public void slideToCenter()
	{
		setUser_intended(UserIntendedGCState.Unmoved);
		slideDirection = SlideDirection.ToCenter;
	}

	public bool is_sliding_up()
	{
		return slideDirection == SlideDirection.Up;
	}

	public bool is_not_sliding()
	{
		return slideDirection == SlideDirection.None;
	}

	public bool is_sliding_2_center()
	{
		return slideDirection == SlideDirection.ToCenter;
	}

	public UserIntendedGCState getUser_intended()
	{
		return user_intended;
	}

	public void setUser_intended(UserIntendedGCState v)
	{
		user_intended = v;
		setAnswerTransparency();
	}

	public void reverse_user_intended()
	{
		if (user_intended == UserIntendedGCState.Opaque)
			setUser_intended(UserIntendedGCState.Transparrent);
		else if (user_intended == UserIntendedGCState.Transparrent)
			setUser_intended(UserIntendedGCState.Opaque);
		else if (user_intended == UserIntendedGCState.Moved)
			setUser_intended(UserIntendedGCState.Unmoved);
		else if (user_intended == UserIntendedGCState.Unmoved)
			setUser_intended(UserIntendedGCState.Moved);
	}

}
