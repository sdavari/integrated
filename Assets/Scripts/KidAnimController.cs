﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KidAnimController : MonoBehaviour
{
	static Animator anim;


	//private float speed = 5.0f;
	//private float rotationSpeed = 75.0f;

    void Start()
    {
		anim = GetComponent<Animator>();

    }

	void Update()
	{
		KidStatus kidStatus = gameObject.GetComponent<KidActivityStatus>().getKidStatus();
		if (kidStatus == KidStatus.Wave)
		{
			// if kid local pos x>-2 and z>-5 => on the right orientation towards user
			anim.SetBool("isWaving", true);
			anim.SetBool("isWalking", false);
		}

		else if (kidStatus == KidStatus.Walk)
		{
			anim.SetBool("isWalking", true);
			anim.SetBool("isWaving", false);
			transform.Translate(0.0f, 0.0f, 2.0f * Time.deltaTime);
			transform.Rotate(0.0f, 135.0f * Time.deltaTime, 0.0f);
		}
		else if (kidStatus == KidStatus.Idle)
		{
			anim.SetBool("isWaving", false);
			anim.SetBool("isWalking", false);
		}

	}
}
