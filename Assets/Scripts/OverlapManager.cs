﻿using UnityEngine;

public class OverlapManager : MonoBehaviour
{
	public GameObject gc;
	private Renderer _renderer;
	private OverlapMode mode;
	private OverlapMode lastMode;
	private SpriteRenderer _sr;
	private Sprite outlinedSprite;
	private Sprite defaultSprite;
	private float initialX, initialZ;
	private float upY = .6f;
	private float slideY = .03f;
	private float transparentColor = 0.08f;
	private GCNBCommonVariables gCNBCommon;
	public bool onTriggerEntered;
	private Collider otherCollider;

	void Start()
	{
		mode = GameObject.Find("TrialManager").GetComponent<TrialManager>().getMode();
		lastMode = mode;
		_sr = gc.GetComponent<SpriteRenderer>();
		set_sprites(_sr.sprite.name);
		gCNBCommon = gc.GetComponent<GCNBCommonVariables>();
		gCNBCommon.reset(mode);

		_renderer = gc.GetComponent<Renderer>();
		initialX = gc.transform.localPosition.x;
		initialZ = gc.transform.localPosition.z;
		onTriggerEntered = false;
		otherCollider = null;
	}

	public void set_sprites(string spriteName)
	{	
		_sr.sprite = Resources.Load<Sprite>("GC/" + spriteName);
		defaultSprite = Resources.Load<Sprite>("GC/" + spriteName);
		outlinedSprite = Resources.Load<Sprite>("GC/" + spriteName + "Outlined");
	}

	void OnTriggerStay(Collider other)
	{
		if (other.gameObject.tag == "ImportantRW")
		{
			onTriggerEntered = true;
			otherCollider = other;
		}

	}

	void FixedUpdate()
	{
		onTriggerEntered = false;
	}

	void Update()
	{
		mode = GameObject.Find("TrialManager").GetComponent<TrialManager>().getMode();

		if (mode != lastMode)
		{
			gCNBCommon.reset(mode);
			lastMode = mode;
		}
		if (onTriggerEntered)
		{
			switch (mode)
			{
				case OverlapMode.ManualMoveDown:
					break;
				case OverlapMode.ManualMoveUp:
					break;
				case OverlapMode.ManualTransparentizeOpaque:
					break;
				case OverlapMode.ManualTransparentizeTransparent:
					break;
				case OverlapMode.AutoTransparentize:
					if (!gCNBCommon.getClickedOn())
					{
						Color color = _renderer.material.color;
						
						color.a = transparentColor;
						_renderer.material.color = color;
						gCNBCommon.setUser_intended(UserIntendedGCState.Transparrent);
					}
					break;
				case OverlapMode.Auto_Move:
					if (!gCNBCommon.getClickedOn())
					{
						gCNBCommon.slideUp();
					}
					break;
				case OverlapMode.Notify:
				case OverlapMode.NotifyMove:
					if (gCNBCommon.getUser_intended() == UserIntendedGCState.Opaque || gCNBCommon.getUser_intended() == UserIntendedGCState.Unmoved)
					{
						if (Time.time % 2 < 1)
						{
							_sr.sprite = outlinedSprite;
						}
						else if (Time.time % 2 >= 1)
						{
							_sr.sprite = defaultSprite;
						}
					}
					else
					{
						_sr.sprite = defaultSprite;
					}
					break;
				default:
					break;
			}
		}
		else                //On Trigger Exit
		{

			if (otherCollider != null)
			{
				switch (mode)
				{
					case OverlapMode.ManualMoveDown:
						break;
					case OverlapMode.ManualMoveUp:
						break;
					case OverlapMode.ManualTransparentizeOpaque:
						break;
					case OverlapMode.ManualTransparentizeTransparent:
						break;
					case OverlapMode.AutoTransparentize:
						if (!gCNBCommon.getClickedOn())
						{
							Color color = _renderer.material.color;
							color.a = 1.0f;
							_renderer.material.color = color;
							gCNBCommon.setUser_intended(UserIntendedGCState.Opaque);
						}
						break;
					case OverlapMode.Auto_Move:
						if (!gCNBCommon.getClickedOn())
							gCNBCommon.slideToCenter();
						break;
					case OverlapMode.Notify:
					case OverlapMode.NotifyMove:
						_sr.sprite = defaultSprite;
						break;
				}
			}
			otherCollider = null;
		}
	}

	void LateUpdate()
	{
		if (gCNBCommon.is_sliding_up())
		{
			if (gc.transform.localPosition.y < upY)
			{
				gc.transform.localPosition = new Vector3(initialX, gc.transform.localPosition.y + slideY, initialZ);
				gCNBCommon.answerObj.transform.localPosition = new Vector3(gCNBCommon.getAnswerInitialPos().x, 
					gCNBCommon.answerObj.transform.localPosition.y + slideY,
					gCNBCommon.getAnswerInitialPos().z);
			}
			else
			{
				gc.transform.localPosition = new Vector3(initialX, upY, initialZ);
				gCNBCommon.answerObj.transform.localPosition = new Vector3(gCNBCommon.getAnswerInitialPos().x, gCNBCommon.upYAns, gCNBCommon.getAnswerInitialPos().z);
				gCNBCommon.stopSlide();
			}
		}
		if (gCNBCommon.is_sliding_2_center())
		{
			if (gc.transform.localPosition.y > 0)
			{
				gc.transform.localPosition = new Vector3(initialX, gc.transform.localPosition.y - slideY, initialZ);
				gCNBCommon.answerObj.transform.localPosition = new Vector3(gCNBCommon.getAnswerInitialPos().x, gCNBCommon.answerObj.transform.localPosition.y - slideY, gCNBCommon.getAnswerInitialPos().z);
			}
			else
			{
				gc.transform.localPosition = new Vector3(initialX, 0, initialZ);
				gCNBCommon.answerObj.transform.localPosition = gCNBCommon.getAnswerInitialPos();
				gCNBCommon.stopSlide();
			}
		}
	}
}

